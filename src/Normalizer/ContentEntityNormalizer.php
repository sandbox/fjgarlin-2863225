<?php

namespace Drupal\clean_rest\Normalizer;

use Drupal\serialization\Normalizer\EntityNormalizer;
use Drupal\Core\Field\FieldItemList;

/**
 * Normalizes/denormalizes Drupal content entities into a clean structure.
 */
class ContentEntityNormalizer extends EntityNormalizer {

  /**
   * Transformers to run output through before returning.
   *
   * @var array
   */
  private $transformers = [
    'article' => '\Drupal\clean_rest\Transformers\BlogTransformer',
    'page' => '\Drupal\clean_rest\Transformers\PageTransformer',
    'client' => '\Drupal\clean_rest\Transformers\ClientTransformer',
    'grid_page' => '\Drupal\clean_rest\Transformers\GridTransformer',
  ];

  /**
   * The interface or class that this Normalizer supports.
   *
   * @var array
   */
  protected $supportedInterfaceOrClass = ['Drupal\Core\Entity\ContentEntityInterface'];

  /**
   * How to normalize the field attribute.
   *
   * Possible values: rendered | raw | cleaner_default.
   *
   * @var string
   */
  protected $attributeMode = 'cleaner_default';

  /**
   * {@inheritdoc}
   */
  public function normalize($object, $format = NULL, array $context = array()) {
    $context += array(
      'account' => NULL,
    );

    $attributes = [];
    // $display = entity_get_display($object->getEntityTypeId(), ...
    // ...$object->bundle(), 'full');
    // $content = $display->build($object);
    // $renderer = \Drupal::service('renderer');.
    foreach ($object as $name => $field) {
      if ($field->access('view', $context['account'])) {
        switch ($this->attributeMode) {
          case 'rendered':
            $field_value = $this->getRenderedFieldValue($object, $field);
            break;

          case 'raw':
            $field_value = $this->getRawFieldValue($object, $field);
            break;

          case 'cleaner_default':
            $field_value = $this->getCleanerDefaultFieldValue($object, $field);
            break;
        }

        $attributes[$name] = $this->serializer->normalize($field_value, $format, $context);
      }
    }

    // If we have a transformer for this kind of object, then run it through
    // the filter and replace the data.
    if (isset($object->type)) {
      $type = $this->getRawFieldValue($object, $object->type);
      if (isset($this->transformers[$type]) and class_exists($this->transformers[$type])) {
        $transformer = new $this->transformers[$type]();
        if (method_exists($transformer, 'transform')) {
          $attributes = $transformer->transform($attributes);
        }
      }
    }

    return $attributes;
  }

  /**
   * Render the field value of an entity in a given view mode.
   *
   * @param mixed $object
   *   Main object.
   * @param mixed $field
   *   Field to retrieve.
   * @param string $view_mode
   *   View mode of the object.
   *
   * @return mixed
   *   Rendered element.
   */
  private function getRenderedFieldValue($object, $field, $view_mode = 'full') {
    $field_name = $field->getName();
    $field_value = $object->$field_name->view($view_mode);
    return drupal_render($field_value);
  }

  /**
   * Render the raw field value of an entity.
   *
   * @param mixed $object
   *   Main object.
   * @param mixed $field
   *   Field to retrieve.
   *
   * @return string
   *   Raw value of the given field.
   */
  private function getRawFieldValue($object, $field) {
    $field_name = $field->getName();
    $raw = '';
    switch ($field_name) {
      case 'uid':
        $raw = $object->getOwnerId();
        break;

      case 'type':
        $raw = $object->getType();
        break;

      default:
        $raw = $object->$field_name->value;
    }
    return $raw;
  }

  /**
   * Same as the default ContentEntityNormalizer but in a cleaner way.
   *
   * @param mixed $object
   *   Main object.
   * @param mixed $field
   *   Field to retrieve.
   *
   * @return mixed
   *   Simplified version of field if applicable.
   */
  private function getCleanerDefaultFieldValue($object, $field) {
    $field_name = $field->getName();
    if ($field instanceof FieldItemList) {

      $multiple = $object->getFieldDefinition($field_name)->getFieldStorageDefinition()->isMultiple();
      if (!$multiple && isset($object->$field_name->value)) {
        return $object->$field_name->value;
      }
    }
    return $field;
  }

}
