<?php

namespace Drupal\clean_rest\Transformers;

use Drupal\Component\Utility\Html;
use GuzzleHttp\Client;

/**
 * Series of helper methods for Transformers.
 */
trait TransformerHelpers {

  /**
   * Replaces relative URL with absolute ones.
   *
   * @param string $content
   *   Content to search the url in.
   * @param string $base_url
   *   Base url to replace.
   *
   * @return mixed
   *   Parsed version of the content with urls replaced.
   */
  public function searchReplaceRelativeToFullUrls($content, $base_url) {
    // http://wintermute.com.au/bits/2005-09/php-relative-absolute-links/
    return preg_replace('#(href|src)="([^:"]*)(?:")#', '$1="' . $base_url . '$2"', $content);
  }

  /**
   * Build a testimonial from the fields given in data.
   *
   * @param mixed $data
   *   Entity containing the testimonial fields.
   *
   * @return array|null
   *   Formatted testimonial.
   */
  public function getTestimonial($data) {
    if (isset($data->field_testimonial_author) and isset($data->field_testimonial_role) and isset($data->field_testimonial_content)) {
      if ($data->field_testimonial_author or $data->field_testimonial_role or $data->field_testimonial_content) {
        $testimonial = [
          'author' => $data->field_testimonial_author ? Html::decodeEntities($data->field_testimonial_author) : NULL,
          'role' => $data->field_testimonial_role ? Html::decodeEntities($data->field_testimonial_role) : NULL,
          'content' => $data->field_testimonial_content ? Html::decodeEntities($data->field_testimonial_content) : NULL,
        ];

        if (isset($data->field_testimonial_image) and $data->field_testimonial_image) {
          $testimonial['image'] = $this->getFieldUrl($data->field_testimonial_image);
        }

        return $testimonial;
      }
    }

    return NULL;
  }

  /**
   * Makes full name out of a team member object.
   *
   * @param mixed $team_member
   *   Data containing the team member information.
   *
   * @return string
   *   Formatted full name of the team member.
   */
  public function getFullName($team_member) {
    if (is_array($team_member)) {
      $team_member = (object) $team_member;
    }

    $full_name = '';

    // Name.
    if (isset($team_member->field_author) and is_string($team_member->field_author)) {
      $full_name .= $team_member->field_author;
    }
    elseif (isset($team_member->title) and is_string($team_member->title)) {
      $full_name .= $team_member->title;
    }

    // Surname.
    if (isset($team_member->field_surname) and is_string($team_member->field_surname)) {
      $full_name .= ' ' . $team_member->field_surname;
    }

    return $full_name;
  }

  /**
   * Http://stackoverflow.com/questions/173400/how-to-check-if-php-array-is-associative-or-sequential.
   *
   * @param array $arr
   *   Array to check.
   *
   * @return bool
   *   Whether the array is associative or not.
   */
  public function isAssoc(array $arr) {
    if (array() === $arr) {
      return FALSE;
    }
    return array_keys($arr) !== range(0, count($arr) - 1);
  }

  /**
   * Extracts URLs from field with multiple values.
   *
   * @param mixed $field
   *   Field to extract URLs from.
   *
   * @return string|array
   *   List of URLs.
   */
  public function getFieldUrls($field) {
    if ($this->isAssoc($field)) {
      return $this->getFieldUrl($field);
    }
    else {
      $urls = [];
      foreach ($field as $item) {
        $urls[] = $this->getFieldUrl($item);
      }

      return $urls;
    }
  }

  /**
   * Extracts url from field.
   *
   * @param mixed $field
   *   Field containing URL.
   *
   * @return string
   *   URL of the field.
   */
  public function getFieldUrl($field) {
    if (isset($field[0])) {
      $field = $field[0];
    }
    $field = (object) $field;

    $url = isset($field->url) ? $field->url : FALSE;
    if (!$url) {
      // For link field type the key is "uri".
      $url = isset($field->uri) ? $field->uri : '';
    }

    return $url;
  }

  /**
   * Gets expanded version of field when it's just a reference.
   *
   * @param mixed $field
   *   Get expanded information via the API.
   *
   * @return array
   *   Field with extra information.
   */
  public function getExpanded($field) {
    global $base_url;

    $expanded = [];
    if (is_array($field) and !$this->isAssoc($field)) {
      foreach ($field as $value) {
        $expanded[] = $this->getExpanded($value);
      }
    }
    else {
      $field = (object) $field;
      if (isset($field->url)) {
        $expanded = $this->getApiData($base_url . $field->url);
      }
      else {
        // NOTE: what do we do here?
      }
    }

    // If only one element and numeric array then "unarray" it.
    if (is_array($expanded) and !$this->isAssoc($expanded) and count($expanded) == 1) {
      $expanded = $expanded[0];
    }

    return $expanded;
  }

  /**
   * Get data returned from API.
   *
   * @param string $url
   *   URL of the API.
   * @param string $format
   *   Format to get the data.
   * @param string $extra_query
   *   Query to send to the API.
   *
   * @return array|mixed
   *   Response from the API.
   */
  public function getApiData($url, $format = 'json', $extra_query = '') {
    $client = new Client(['http_errors' => FALSE, 'verify' => FALSE]);
    $res = $client->get($url . '?_format=' . $format . '&' . $extra_query);
    $info = ($res->getStatusCode() == 200) ?
            json_decode($res->getBody()) :
            [];

    return $info;
  }

  /**
   * Takes array of tags and flattens it into a string.
   *
   * @param mixed $tags
   *   List of tags.
   * @param string $property
   *   Property to check within the tag objects.
   * @param string $glue
   *   Glue to use when imploding the tags.
   *
   * @return string
   *   Flat representation of the tags.
   */
  protected function flattenTags($tags, $property = 'name', $glue = ', ') {
    // If only one item.
    if (isset($tags->{$property})) {
      return $tags->{$property};
    }

    // Multiple items.
    $tags_array = [];
    foreach ($tags as $tag) {
      $tags_array[] = $tag->{$property};
    }

    return implode($glue, $tags_array);
  }

  /**
   * Takes array of tags and flattens it into a simpler array.
   *
   * @param mixed $tags
   *   List of tags.
   * @param string $property
   *   Property within the tag object.
   *
   * @return array
   *   Single array with tags.
   */
  protected function flattenTagsArray($tags, $property = 'name') {
    // If only one item.
    if (isset($tags->{$property})) {
      return $tags->{$property};
    }

    // Multiple items.
    $tags_array = [];
    foreach ($tags as $tag) {
      $tags_array[] = $tag->{$property};
    }

    return $tags_array;
  }

  /**
   * Gets the string color representation of a color field.
   *
   * @param mixed $field
   *   Color field information.
   *
   * @return string
   *   Representation of the given color in rgba.
   */
  protected function getColor($field) {
    return !empty($field) ?
            $this->hex2rgba($field[0]['color'], $field[0]['opacity']) :
            '';
  }

  /**
   * Http://mekshq.com/how-to-convert-hexadecimal-color-code-to-rgb-or-rgba-using-php.
   */
  protected function hex2rgba($color, $opacity = FALSE) {

    $default = 'rgb(0,0,0)';

    // Return default if no color provided.
    if (empty($color)) {
      return $default;
    }

    // Sanitize $color if "#" is provided.
    if ($color[0] == '#') {
      $color = substr($color, 1);
    }

    // Check if color has 6 or 3 characters and get values.
    if (strlen($color) == 6) {
      $hex = array(
        $color[0] . $color[1],
        $color[2] . $color[3],
        $color[4] . $color[5],
      );
    }
    elseif (strlen($color) == 3) {
      $hex = array(
        $color[0] . $color[0],
        $color[1] . $color[1],
        $color[2] . $color[2],
      );
    }
    else {
      return $default;
    }

    // Convert hexadec to rgb.
    $rgb = array_map('hexdec', $hex);

    // Check if opacity is set(rgba or rgb)
    if ($opacity) {
      if (abs($opacity) > 1) {
        $opacity = 1.0;
      }
      $output = 'rgba(' . implode(",", $rgb) . ',' . $opacity . ')';
    }
    else {
      $output = 'rgb(' . implode(",", $rgb) . ')';
    }

    // Return rgb(a) color string.
    return $output;
  }

  /**
   * Trims text to a space then adds ellipses if desired.
   *
   * @param string $input
   *   Text to trim.
   * @param int $length
   *   In characters to trim to.
   * @param bool $ellipses
   *   If ellipses (...) are to be added.
   * @param bool $strip_html
   *   If html tags are to be stripped.
   *
   * @see http://www.ebrueggeman.com/blog/abbreviate-text-without-cutting-words-in-half
   *
   * @return string
   *   Trimmed text.
   */
  protected function trimText($input, $length, $ellipses = TRUE, $strip_html = TRUE) {
    // Strip tags, if desired.
    if ($strip_html) {
      $input = strip_tags($input);
    }

    // No need to trim, already shorter than trim length.
    if (strlen($input) <= $length) {
      return $input;
    }

    // Find last space within length.
    $last_space = strrpos(substr($input, 0, $length), ' ');
    $trimmed_text = substr($input, 0, $last_space);

    // Add ellipses (...)
    if ($ellipses) {
      $trimmed_text .= '...';
    }

    return $trimmed_text;
  }

  /**
   * Drupal is not serving metatags via JSON if they are the default ones.
   *
   * @param mixed $item
   *   Node information.
   *
   * @return mixed
   *   Metatags linked to the node.
   */
  protected function computeMetatags($item) {
    $metatags = unserialize($item->field_metatags);
    if (empty($metatags)) {
      $metatags = [];
    }

    // Get default metatags configuration.
    $default_metatags = [];
    $metatag_attachments = metatag_get_tags_from_route();
    if ($metatag_attachments) {
      // Extract the attributs.
      if (!empty($metatag_attachments['#attached']['html_head'])) {
        foreach ($metatag_attachments['#attached']['html_head'] as $attachment) {
          if (!empty($attachment[1])) {
            $default_metatags[$attachment[1]] = html_entity_decode($attachment[0]['#attributes']['content'], ENT_QUOTES);
          }
        }

        // Prepend page title as it's not doing it...
        if (isset($default_metatags['title']) and $default_metatags['title'][0] == '|') {
          $default_metatags['title'] = $item->title . ' ' . $default_metatags['title'];
        }
      }
    }

    $metatags['title'] = $metatags['title'] ?? $default_metatags['title'] ?? NULL;
    $metatags['description'] = $metatags['description'] ?? $default_metatags['description'] ?? NULL;

    return $metatags;
  }

}
