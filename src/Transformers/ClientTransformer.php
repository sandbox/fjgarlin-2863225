<?php

namespace Drupal\clean_rest\Transformers;

/**
 * Class to transform the Client content type output.
 */
class ClientTransformer {
  use TransformerHelpers;

  /**
   * Transforms a given Drupal API output to the expected by a blog post.
   *
   * @param mixed $item
   *   Page node to transform.
   *
   * @return array
   *   Transformed array to send to the output.
   */
  public function transform($item) {
    if (is_array($item)) {
      $item = (object) $item;
    }

    global $base_url;

    // Build final array.
    $transformed = [
      'id' => $item->nid,
      'logo' => $this->getFieldUrl($item->field_logo),
      'alternative_logo' => $this->getFieldUrl($item->field_grey_logo) ?: $this->getFieldUrl($item->field_logo),
      'name' => $item->title,
      'description' => $this->searchReplaceRelativeToFullUrls($item->field_description, $base_url),
    ];

    return $transformed;
  }

}
