<?php

namespace Drupal\clean_rest\Transformers;

/**
 * Transformer class for Page content type.
 */
class PageTransformer {
  use TransformerHelpers;

  /**
   * Transforms a given Drupal API output to the expected by a blog post.
   *
   * @param mixed $item
   *   Page node to transform.
   *
   * @return array
   *   Transformed array to send to the output.
   */
  public function transform($item) {
    if (is_array($item)) {
      $item = (object) $item;
    }

    global $base_url;

    // Build final array.
    $transformed = [
      "meta" => $this->computeMetatags($item),
      "title" => $item->title,
      "subtitle" => $item->field_subtitle,
      "content" => $this->searchReplaceRelativeToFullUrls($item->body, $base_url),
      "_transformed" => TRUE,
    ];

    return $transformed;
  }

}
