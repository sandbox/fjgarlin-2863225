<?php

namespace Drupal\clean_rest\Transformers;

use Drupal;
use Drupal\Component\Utility\Html;

/**
 * Transform Article content type.
 */
class BlogTransformer {
  use TransformerHelpers;

  /**
   * Returns the short version of articles needed for listings.
   *
   * @param mixed $article
   *   Original node.
   *
   * @return array
   *   Short version of the node.
   */
  protected function getShortVersion($article) {
    if (!is_object($article)) {
      $article = (object) $article;
    }

    return [
      'id' => $article->nid,
      'author' => Html::decodeEntities($this->getFullName($article)),
      'author_photo' => $article->field_author_image,
      'image' => isset($article->field_image) ? $article->field_image : NULL,
      'title' => $article->title,
      'date' => strtotime($article->field_publish),
      'category' => $article->field_tags,
      'video' => (bool) $article->field_video_blog,
      'uri' => '/blog/' . trim($article->path, '/'),
      'excerpt' => $article->body,
    ];
  }

  /**
   * Transforms a given Drupal API output to the expected by a blog post.
   *
   * @param mixed $item
   *   Node to transform.
   *
   * @return array
   *   Transformed output.
   */
  public function transform($item) {
    if (is_array($item)) {
      $item = (object) $item;
    }

    global $base_url;

    // Get author fields.
    $author = $this->getExpanded($item->field_author);
    $tags = $this->getExpanded($item->field_tags);
    $tags = $this->flattenTags($tags);

    // Drupal is not returning the alias (the field is there but is empty,
    // even when we have content in it...)
    // Using Drupal request is not making it very abstract and testable...
    // the bug might be corrected in drupal soon
    // https://www.drupal.org/node/2693077
    $url = "/blog/" . trim(Drupal::request()->getPathInfo(), '/');

    // Get related data and get the short version of it.
    $related = $this->getApiData($base_url . '/blog-related', 'json', 'id=' . $item->nid);
    $related = array_map([$this, 'getShortVersion'], $related);
    if (empty($related)) {
      $related = FALSE;
    }

    // Build final array.
    $transformed = [
      "id" => $item->nid,
      "meta" => $this->computeMetatags($item),
      "author" => $this->getFullName($author),
      "author_photo" => $this->getFieldUrl($author->field_image),
      "date" => strtotime($item->field_publish),
      "image" => $this->getFieldUrl($item->field_image),
      "category" => $tags,
      "video" => (bool) $item->field_video_blog,
      "title" => $item->title,
      "excerpt" => $this->trimText($item->body, 250),
      "uri" => $url,
      "leading_content" => $this->searchReplaceRelativeToFullUrls($item->body, $base_url),
      "content" => $this->searchReplaceRelativeToFullUrls($item->field_content, $base_url),
      "zoho" => $item->field_zoho,
      "testimonial" => $this->getTestimonial($item),
      "related" => $related,
      "type" => "blog",
      "_transformed" => TRUE,
    ];

    return $transformed;
  }

}
