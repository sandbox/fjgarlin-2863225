<?php

namespace Drupal\clean_rest\Transformers;

/**
 * Transforms Grid content type.
 */
class GridTransformer {
  use TransformerHelpers;

  /**
   * Transforms a given Drupal API output to the expected by a blog post.
   *
   * @param mixed $item
   *   Node to transform.
   *
   * @return array
   *   Transformed node.
   */
  public function transform($item) {
    if (is_array($item)) {
      $item = (object) $item;
    }

    // Get tiles.
    $tiles = [];
    if (is_array($item->field_tiles)) {
      foreach ($item->field_tiles as $tile) {
        $tmp = $this->getTile($tile);
        if ($tmp) {
          $tiles[$tmp['tweaks']['tile_setting']][] = $tmp;
        }
      }
    }

    global $base_url;

    $tags = $this->getExpanded($item->field_page_tags);
    $tags = $this->flattenTags($tags);

    // Build final array.
    $transformed = [
      "meta" => $this->computeMetatags($item),
      "title" => $item->title,
      "subtitle" => $item->field_subtitle,
      "content" => $this->searchReplaceRelativeToFullUrls($item->body, $base_url),
      "tags" => $tags,
      "tiles" => $tiles,
      "_transformed" => TRUE,
    ];

    return $transformed;
  }

  /**
   * Gets a 'tile' with all the dependencies of it.
   */
  private function getTile($tile) {
    if (!is_object($tile)) {
      $tile = (object) $tile;
    }

    global $base_url;
    $content = $tile->field_content[0];
    if (!isset($content['url'])) {
      return FALSE;
    }

    $content = $this->getApiData($base_url . $content['url']);
    if (!$content) {
      return FALSE;
    }
    $slug = trim($content->uri, '/');
    $slug = str_replace($content->type . '/', '', $slug);

    $category = ucwords($content->type);

    $client = NULL;
    if (isset($content->client)) {
      $client = (object) [
        "logo" => $content->client_logo,
        "name" => $content->client,
      ];
    }

    $author = FALSE;
    if (isset($content->author)) {
      $author = (object) [
        "image" => $content->author_photo,
        "name" => $content->author,
      // Role not available and not shown when it's a blog post.
        "role" => date("d M Y", $content->date),
      ];
    }

    $image = isset($content->tile_image) ? $content->tile_image : $content->image;
    if (!$tile->field_image_over_text) {
      $image = FALSE;
    }

    return [
      "id" => $content->id,
      "title" => $content->title,
      "slug" => $slug,
      "image" => $image,
      "category" => $category,
      "excerpt" => $content->excerpt,
      "client_logo" => $client ? $client->logo : FALSE,
      "author" => $author,
      "type" => $content->type,
      "tweaks" => [
        "image_over_text" => (bool) $tile->field_image_over_text,
        "tile_setting" => $tile->field_tile_setting,
        "box_color" => $this->getColor($tile->field_box_color),
        "title_color" => $this->getColor($tile->field_title_color),
        "text_color" => $this->getColor($tile->field_text_color),
      ],
    ];
  }

}
